# Title of a document with an image from subchapter 1 A - Example 1

This document contains an image in English.

## Automatic media / link / asset localization - Example 1

![English flag](static/english_flag.png)

This image source is dynamically localized while still being referenced in the markdown source of the page as `![something](image.png)`. The image has to be located in the same folder as the document.
