# Chapter 2 - Example 1

This is the second chapter in English.

## Section 1 - Example 1

This is the first section in the second chapter

Here is some **bold text**. Here is some *italic text*.

MkDocs provides a [brief guide on writhing with Markdown](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown){target=_blank}.

## Section 2 - Example 1

This is the second section in the second chapter.

### Subsection 1 - Example 1

This is the first subsection in the second section in the second chapter.

#### Subsubsection 1 - Example 1

This is the first subsubsection in the first subsection in second section in the second chapter.

##### Level 5 - Example 1

There is yet another level (5th) of content structure.

###### Level 6 - Example 1

There is a final level (6th) of content structure.
